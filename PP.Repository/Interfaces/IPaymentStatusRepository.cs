﻿using PP.Entity.Entities;
using PP.Entity.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PP.Repository.Interfaces
{
    public interface IPaymentStatusRepository : IBaseRepository<PaymentStatus>
    {
        #region CRUD
        #endregion
        Task<int> AddPaymentStatusAsync(PaymentStatusViewModel model);
        Task<List<PaymentStatusViewModel>> GetAllPaymentStatussAsync();
        Task<PaymentStatusViewModel> GetPaymentStatusByPaymentStatusIdAsync(int PaymentStatusId);
        Task<int> UpdatePaymentStatusAsync(int Id, PaymentStatusViewModel PaymentStatus);

    }
}
