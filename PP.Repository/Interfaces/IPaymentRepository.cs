﻿using PP.Entity.Entities;
using PP.Entity.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PP.Repository.Interfaces
{
    public interface IPaymentRepository : IBaseRepository<Payment>
    {
        #region CRUD
        #endregion
        Task<int> AddPaymentAsync(RequestPaymentViewModel model);
        Task<List<PaymentViewModel>> GetAllPaymentsAsync();
        Task<PaymentViewModel> GetPaymentByPaymentIdAsync(int PaymentId);
        Task<ResponsePaymentViewModel> GetPaymentResponseByPaymentIdAsync(int PaymentId);
        Task<int> UpdatePaymentAsync(int Id, PaymentViewModel Payment);

    }
}
