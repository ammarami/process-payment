﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PP.Common;
using PP.Common.Helpers;
using PP.Entity.Entities;
using PP.Entity.ViewModels;
using PP.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PP.Repository.Repositories
{
    public class PaymentRepository : BaseRepository<Payment>, IPaymentRepository
    {

        #region Initialization
        private readonly IHttpContextAccessor _httpContextAccessor;
        protected new readonly PPContext _dbContext;
        public PaymentRepository(PPContext dbContext, IHttpContextAccessor httpContextAccessor) : base(dbContext, httpContextAccessor)

        {
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
        }
        #endregion

        #region CRUD
        public async Task<int> AddPaymentAsync(RequestPaymentViewModel model)
        {
            var PaymentObj = new Payment();
            var PaymentStatusObj = new PaymentStatus();
            CopyViewModelToEntity(model, PaymentObj);
            _dbContext.Entry(PaymentObj).State = EntityState.Added;
            var result = await _dbContext.SaveChangesAsync();
            PaymentStatusObj.PaymentId = PaymentObj.Id;
            PaymentStatusObj.PaymentStatusValue = PaymentStatusEnum.Processed;
            _dbContext.Entry(PaymentStatusObj).State = EntityState.Added;
            await _dbContext.SaveChangesAsync();
            return PaymentObj.Id;
        }

        public async Task<List<PaymentViewModel>> GetAllPaymentsAsync()
        {
            return await _dbContext.Payments.Include(pi => pi.Status).Select(pi => new PaymentViewModel()
            {
                Id = pi.Id,
                CreditCardNumber = pi.CreditCardNumber,
                CardHolder = pi.CardHolder,
                ExpirationDate = pi.ExpirationDate,
                SecurityCode = pi.SecurityCode,
                Amount = pi.Amount,
                Status = StatusProvider.GetStatus((int)pi.Status.PaymentStatusValue)
            }).ToListAsync();
        }

        public async Task<PaymentViewModel> GetPaymentByPaymentIdAsync(int PaymentId)
        {
            return await _dbContext.Payments.Include(pi => pi.Status).Select(pi => new PaymentViewModel()
            {
                Id = pi.Id,
                CreditCardNumber = pi.CreditCardNumber,
                CardHolder = pi.CardHolder,
                ExpirationDate = pi.ExpirationDate,
                SecurityCode = pi.SecurityCode,
                Amount = pi.Amount,
                Status = StatusProvider.GetStatus((int)pi.Status.PaymentStatusValue)
            }).FirstOrDefaultAsync();
        }

        public async Task<ResponsePaymentViewModel> GetPaymentResponseByPaymentIdAsync(int PaymentId)
        {
            return await _dbContext.Payments.Include(pi => pi.Status).Select(pi => new ResponsePaymentViewModel()
            {
                Id = pi.Id,
                CreditCardNumber = pi.CreditCardNumber,
                CardHolder = pi.CardHolder,
                ExpirationDate = pi.ExpirationDate,
                SecurityCode = pi.SecurityCode,
                Amount = pi.Amount,
                Status = StatusProvider.GetStatus((int)pi.Status.PaymentStatusValue)
            }).FirstOrDefaultAsync();
        }

        public async Task<int> UpdatePaymentAsync(int Id, PaymentViewModel model)
        {
            Payment PaymentObj = await _dbContext.Payments.Where(x => x.Id == Id).FirstOrDefaultAsync();
            if (PaymentObj != null)
            {
                CopyViewModelToEntity(model, PaymentObj);
                _dbContext.Entry(PaymentObj).State = EntityState.Modified;
                return await _dbContext.SaveChangesAsync();
            }
            return 0;
        }
        #endregion

    }
}
