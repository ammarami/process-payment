﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.ComponentModel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PP.Repository.Interfaces;
using PP.Entity.Entities;

namespace PP.Repository.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        #region Properties

        private readonly IHttpContextAccessor _httpContextAccessor;

        protected readonly PPContext _dbContext;


        #endregion

        #region Constructor


        public BaseRepository(PPContext dbContext)

        {
            _dbContext = dbContext;
        }


        public BaseRepository(PPContext dbContext, IHttpContextAccessor httpContextAccessor)

        {
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Interface Methods

        public virtual async Task<T> GetById(int id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public virtual async Task<IEnumerable<T>> List()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> List(params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _dbContext.Set<T>();
            return await includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty)).ToListAsync();
        }
        public virtual async Task<IEnumerable<T>> List(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _dbContext.Set<T>();
            query = query.Where(predicate);
            return await includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty)).ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> List(Expression<Func<T, bool>> predicate, Expression<Func<T, string>> orderBy, int pageSize = 15, int pageNumber = 1, params Expression<Func<T, object>>[] includes)
        {

            IQueryable<T> query = _dbContext.Set<T>();
            query = query.Where(predicate).OrderBy(orderBy).Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return await includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty)).ToListAsync();
        }

        public virtual async Task<int> Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
           return await _dbContext.SaveChangesAsync();
            
        }
        public virtual async Task<int> Update(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            return await _dbContext.SaveChangesAsync();
        }

        public virtual async Task<int> Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            return await _dbContext.SaveChangesAsync();
        }


        #endregion

        #region ModelCopier
        public void CopyEntityToViewModel(object from, object to)
        {
            CopyModel(from, to);
        }

        public void CopyViewModelToEntity(object from, object to)
        {
            CopyModel(from, to);
        }

        public void CopyEntityToEntity(object from, object to)
        {
            CopyModel(from, to);
        }

        private static void CopyModel(object from, object to)
        {
            if (from == null || to == null)
            {
                return;
            }
            PropertyDescriptorCollection fromProperties = TypeDescriptor.GetProperties(from);
            PropertyDescriptorCollection toProperties = TypeDescriptor.GetProperties(to);
            foreach (PropertyDescriptor fromProperty in fromProperties)
            {
                PropertyDescriptor toProperty = toProperties.Find(fromProperty.Name, true /* ignoreCase */);
                if (toProperty != null && !toProperty.IsReadOnly)
                {
                    bool isDirectlyAssignable = toProperty.PropertyType.IsAssignableFrom(fromProperty.PropertyType);
                    bool liftedValueType = (isDirectlyAssignable) ? false : (Nullable.GetUnderlyingType(fromProperty.PropertyType) == toProperty.PropertyType);
                    if (isDirectlyAssignable || liftedValueType)
                    {
                        object fromValue = fromProperty.GetValue(from);
                        if (isDirectlyAssignable || (fromValue != null && liftedValueType))
                        {
                            toProperty.SetValue(to, fromValue);
                        }
                    }
                }
            }
        }
        #endregion

    }
}
