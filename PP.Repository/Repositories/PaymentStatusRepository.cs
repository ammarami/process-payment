﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PP.Entity.Entities;
using PP.Entity.ViewModels;
using PP.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PP.Repository.Repositories
{
    public class PaymentStatusRepository : BaseRepository<PaymentStatus>, IPaymentStatusRepository
    {

        #region Initialization
        private readonly IHttpContextAccessor _httpContextAccessor;
        protected new readonly PPContext _dbContext;
        public PaymentStatusRepository(PPContext dbContext, IHttpContextAccessor httpContextAccessor) : base(dbContext, httpContextAccessor)

        {
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
        }
        #endregion

        #region CRUD
        public async Task<int> AddPaymentStatusAsync(PaymentStatusViewModel model)
        {
            var PaymentStatusObj = new PaymentStatus();
            CopyViewModelToEntity(model, PaymentStatusObj);
            _dbContext.Entry(PaymentStatusObj).State = EntityState.Added;
            var result = await _dbContext.SaveChangesAsync();
            return result;
        }

        public async Task<List<PaymentStatusViewModel>> GetAllPaymentStatussAsync()
        {
            return await _dbContext.PaymentStatuses.Include(p => p.Payment).Select(pi => new PaymentStatusViewModel()
            {
                PaymentStatusId = pi.Id,
                PaymentStatusValue = pi.PaymentStatusValue
            }).ToListAsync();
        }

        public async Task<PaymentStatusViewModel> GetPaymentStatusByPaymentStatusIdAsync(int PaymentStatusId)
        {
            PaymentStatusViewModel model = new PaymentStatusViewModel();
            var PaymentStatusObj = await _dbContext.PaymentStatuses.FirstOrDefaultAsync(p => p.Id == PaymentStatusId);
            CopyEntityToViewModel(PaymentStatusObj, model);
            return model;
        }

        public async Task<int> UpdatePaymentStatusAsync(int Id, PaymentStatusViewModel model)
        {
            PaymentStatus PaymentStatusObj = await _dbContext.PaymentStatuses.Where(x => x.Id == Id).FirstOrDefaultAsync();
            if (PaymentStatusObj != null)
            {
                CopyViewModelToEntity(model, PaymentStatusObj);
                _dbContext.Entry(PaymentStatusObj).State = EntityState.Modified;
                return await _dbContext.SaveChangesAsync();
            }
            return 0;
        }
        #endregion

    }
}
