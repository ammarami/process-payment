﻿
namespace PP.Common.Helpers
{
    public static class StatusProvider
    {
        public static string GetStatus(int val)
        {
            string status = "";

            switch (val)
            {
                case 1:
                    return "Pending";
                case 2:
                    return "Processed";
                case 3:
                    return "Failed";
            }

            return status;
        }
    }
}
