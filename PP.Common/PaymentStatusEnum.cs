﻿
namespace PP.Common
{
    /// <summary>
    /// Payment Status
    /// </summary>
    public enum PaymentStatusEnum
    {
        Pending = 1,
        Processed = 2,
        Failed = 3
    }
}
