﻿
namespace PP.Common
{
    /// <summary>
    /// Generic Conctants
    /// </summary>
    public class Constants
    {
       
        public const string CONNECTION_STRING = "Server=(localdb)\\MSSQLLocalDB;Initial Catalog=Payment-Processing;Integrated Security=True;";
        public const string PRODUCTION_VERSION = "2.2.4-servicing-10062";
        public const string SUCCESS = "Payment is processed";
        public const string ERROR400 = "The request is invalid";
        public const string ERROR500 = "500 internal server error";
        public const string CHEAP_PAYMENTS_RESPONSE = "Payment succeeded with cheap gateway";
        public const string EXPENSIVE_PAYMENTS_RESPONSE = "Payment succeeded with expensive gateway";
        public const string PREMIUM_PAYMENTS_RESPONSE = "Payment succeeded with premium gateway";
        public const string EXTERNAL_SERVICE_API = "http://localhost:64651/api/ProcessPayment";
    }
}
