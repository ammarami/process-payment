﻿
using PP.Common;
using PP.ExternalServices.ViewModels;

namespace PP.ExternalServices.Services
{
    public class CheapPaymentGateway : ICheapPaymentGateway
    {
        public string ProcessPayment(RequestPaymentViewModel model)
        {
            return Constants.CHEAP_PAYMENTS_RESPONSE;
        }
    }
}
