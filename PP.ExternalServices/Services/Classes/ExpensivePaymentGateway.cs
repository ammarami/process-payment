﻿
using PP.Common;
using PP.ExternalServices.ViewModels;

namespace PP.ExternalServices.Services
{
    public class ExpensivePaymentGateway : IExpensivePaymentGateway
    {
        public string ProcessPayment(RequestPaymentViewModel model)
        {
            return Constants.EXPENSIVE_PAYMENTS_RESPONSE;
        }
    }
}
