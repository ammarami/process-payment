﻿
using PP.Common;
using PP.ExternalServices.ViewModels;

namespace PP.ExternalServices.Services
{
    public class PremiumPaymentService : IPremiumPaymentService
    {
        public string ProcessPayment(RequestPaymentViewModel model)
        {
            return Constants.PREMIUM_PAYMENTS_RESPONSE;
        }
    }
}
