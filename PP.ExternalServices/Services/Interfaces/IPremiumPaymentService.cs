﻿using PP.ExternalServices.ViewModels;

namespace PP.ExternalServices.Services
{
    public interface IPremiumPaymentService
    {
        string ProcessPayment(RequestPaymentViewModel model);
        
    }
}
