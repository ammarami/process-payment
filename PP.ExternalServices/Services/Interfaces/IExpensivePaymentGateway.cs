﻿using PP.ExternalServices.ViewModels;

namespace PP.ExternalServices.Services
{
    public interface IExpensivePaymentGateway
    {
        string ProcessPayment(RequestPaymentViewModel model);
        
    }
}
