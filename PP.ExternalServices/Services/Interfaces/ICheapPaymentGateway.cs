﻿using PP.ExternalServices.ViewModels;

namespace PP.ExternalServices.Services
{
    public interface ICheapPaymentGateway
    {
        string ProcessPayment(RequestPaymentViewModel model);
        
    }
}
