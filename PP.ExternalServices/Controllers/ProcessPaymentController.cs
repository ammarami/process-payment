﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using PP.ExternalServices.Services;
using PP.ExternalServices.ViewModels;

namespace SenSights_API.Controllers
{
    /// <summary>
    /// Controller for External Services
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessPaymentController : ControllerBase
    {
        #region Initialization
        private ICheapPaymentGateway _cheapPaymentService;
        private IExpensivePaymentGateway _expensivePaymentGateway;
        private IPremiumPaymentService _premiumPaymentService;

        public ProcessPaymentController(ICheapPaymentGateway cheapPaymentService,
            IExpensivePaymentGateway expensivePaymentGateway,
            IPremiumPaymentService premiumPaymentService)
        {
            _cheapPaymentService = cheapPaymentService;
            _expensivePaymentGateway = expensivePaymentGateway;
            _premiumPaymentService = premiumPaymentService;
        }
        #endregion

        #region Processing
        // GET: api/Payment/{Id}
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                
                return Ok("hello world");
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        //POST: api/ProcessPayment
       [HttpPost()]
       [Produces("application/json")]
       [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]

        public IActionResult Post(RequestPaymentViewModel model)
        {
            try
            {
                string result = "";
                if (ModelState.IsValid)
                {
                    if (model.Amount <= 20)
                    {
                        result = _cheapPaymentService.ProcessPayment(model);
                    }
                    else if(model.Amount >= 21 && model.Amount <= 500)
                    {
                        if (_expensivePaymentGateway != null)
                        {
                            result = _expensivePaymentGateway.ProcessPayment(model);
                        }
                        else
                        {
                            result = _cheapPaymentService.ProcessPayment(model);
                        }
                    }
                    else if(model.Amount > 500)
                    {
                        for (int i = 0; i <= 2; i++)
                        {
                            result = _premiumPaymentService.ProcessPayment(model);
                            if (!string.IsNullOrWhiteSpace(result))
                            {
                                break;
                            }
                        }
                    }
                    return new OkObjectResult(result);
                }
                return Conflict();
            }
            catch (Exception)
            {

                return BadRequest();

            }

        }

        #endregion

    }
}