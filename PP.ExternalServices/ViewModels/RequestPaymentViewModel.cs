﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PP.ExternalServices.ViewModels
{
    public class RequestPaymentViewModel
    {
        [Required(ErrorMessage = "CreditCardNumber is required")]
        [CreditCard]
        public string CreditCardNumber { get; set; }

        [Required(ErrorMessage = "CardHolder is required")]
        public string CardHolder { get; set; }

        [Required(ErrorMessage = "ExpirationDate is required")]
        public DateTime ExpirationDate { get; set; }

        [StringLength(3, MinimumLength = 3, ErrorMessage = "Maximum 3 characters for security code")]
        public string SecurityCode { get; set; }

        [Required(ErrorMessage = "Amount is required")]
        [Range(0.01, 999999999, ErrorMessage = "Amount must be greater than 0")]
        public decimal Amount { get; set; }
    }
}
