﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PP.Entity.Helpers.CreditCardAttribute
{
    /// <summary>
    /// Using (Luhn) algorithms to validate credit card
    /// </summary>
    public class CustomCreditCardAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string creditCardNumber = (string)value;

            if (string.IsNullOrEmpty(creditCardNumber))
            {
                return new ValidationResult("Credit Card is invalid!");
            }


            int sumOfDigits = creditCardNumber.Where((e) => e >= '0' && e <= '9')
                            .Reverse()
                            .Select((e, i) => ((int)e - 48) * (i % 2 == 0 ? 1 : 2))
                            .Sum((e) => e / 10 + e % 10);

            if (sumOfDigits % 10 == 0)
            {
                return ValidationResult.Success;
            }
            return new ValidationResult("Credit Card is invalid!");



        }
    }
}
