﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PP.Entity.Helpers.CustomDateAttribute
{
    /// <summary>
    /// Expiration Date Validation
    /// </summary>
    public class CustomDateAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            value = (DateTime)value;
            if (DateTime.Now.CompareTo(value) <= 0)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Expiration Date is invalid!");
            }
        }
    }
}
