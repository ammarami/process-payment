﻿using Microsoft.EntityFrameworkCore;
using PP.Common;

namespace PP.Entity.Entities
{
    public class PPContext : DbContext
    {
        private string _connectionString = Constants.CONNECTION_STRING;
        public PPContext(DbContextOptions<PPContext> options)
            : base(options)
        {
            Database.Migrate();
        }

        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<PaymentStatus> PaymentStatuses { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //optionsBuilder.UseSqlServer("Server=localhost;Database=SenSights;Integrated Security=True;");

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasAnnotation("ProductVersion", Constants.PRODUCTION_VERSION);

            // Configure PaymentId as FK for PaymentStatus
            modelBuilder.Entity<Payment>()
                        .HasOne(p => p.Status)
                        .WithOne(ps => ps.Payment)
                        .HasForeignKey<PaymentStatus>(s => s.PaymentId);

        }
    }
}
