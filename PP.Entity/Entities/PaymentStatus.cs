﻿using PP.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace PP.Entity.Entities
{
    public class PaymentStatus
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }
        public PaymentStatusEnum? PaymentStatusValue { get; set; }
        public int PaymentId { get; set; }
        public virtual Payment Payment { get; set; }


    }
}
