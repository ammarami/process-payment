﻿
using PP.Common;

namespace PP.Entity.ViewModels
{
    public class PaymentStatusViewModel
    {
        public int PaymentStatusId { get; set; }
        public PaymentStatusEnum? PaymentStatusValue { get; set; }

    }
}
