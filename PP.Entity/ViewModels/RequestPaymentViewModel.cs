﻿
using PP.Entity.Helpers.CreditCardAttribute;
using PP.Entity.Helpers.CustomDateAttribute;
using System;
using System.ComponentModel.DataAnnotations;

namespace PP.Entity.ViewModels
{
    public class RequestPaymentViewModel
    {
        [Required(ErrorMessage = "CreditCardNumber is required")]
        [CreditCard]
        [CustomCreditCard]
        public string CreditCardNumber { get; set; } = "378282246310005";

        [Required(ErrorMessage = "CardHolder is required")]
        public string CardHolder { get; set; } = "Stuart Dougals";

        [Required(ErrorMessage = "ExpirationDate is required")]
        [CustomDate]
        public DateTime ExpirationDate { get; set; }

        [StringLength(3, MinimumLength = 3, ErrorMessage = "Maximum 3 characters for security code")]
        public string SecurityCode { get; set; } = "123";

        [Required(ErrorMessage = "Amount is required")]
        [Range(0.01, 999999999, ErrorMessage = "Amount must be greater than 0")]
        public decimal Amount { get; set; } = 10;
    }
}
