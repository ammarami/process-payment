﻿using System.ComponentModel.DataAnnotations;
using Xunit;
using System.Linq;


namespace PP.UnitTests
{
    public class CreditCardValidator
    {
        [Fact]
        public void Is_CreditCard_Valid()
        {
            bool result = IsValid("378282246310005");
            Assert.True(result);
        }
        [Fact]
        public void InValid_CreditCard_Valid()
        {
            bool result = IsValid("37828224631005");
            Assert.True(result);
        }
        /// <summary>
        /// Using (Luhn) algorithms to validate credit card
        /// </summary>
        public bool IsValid(string value)
        {
            bool result = false;
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }
            int sumOfDigits = value.Where((e) => e >= '0' && e <= '9')
                            .Reverse()
                            .Select((e, i) => ((int)e - 48) * (i % 2 == 0 ? 1 : 2))
                            .Sum((e) => e / 10 + e % 10);

            if (sumOfDigits % 10 == 0)
            {
                return true;
            }
            return result;

        }
    }

}
