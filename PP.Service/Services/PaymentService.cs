﻿using Newtonsoft.Json;
using PP.Common;
using PP.Entity.ViewModels;
using PP.Repository.Interfaces;
using PP.Service.Interfaces;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PP.Service.Services
{
    public class PaymentService : IPaymentService
    {


        #region Initialization
        private IPaymentRepository _paymentRepository;
        public PaymentService(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;

        }
        #endregion

        #region CRUD
        /// <summary>
        /// Calling External Gateway APIs.
        /// On success saving record to DB
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponsePaymentViewModel> AddPaymentAsync(RequestPaymentViewModel model)
        {
            ResponsePaymentViewModel responseModel = new ResponsePaymentViewModel();
            var json = JsonConvert.SerializeObject(model);
            string url = Constants.EXTERNAL_SERVICE_API;
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            using (HttpClient client = new HttpClient())
            {
                var response = await client.PostAsync(url, stringContent);
                string result = response.Content.ReadAsStringAsync().Result;
                if (response.IsSuccessStatusCode)
                {
                    //Inserting data and getting response model
                    int paymentId = await _paymentRepository.AddPaymentAsync(model);
                    responseModel = await GetPaymentResponseByPaymentIdAsync(paymentId);
                    responseModel.Message = Constants.SUCCESS;
                    responseModel.Gateway = result;
                }
            }
            return responseModel;
        }
        public async Task<List<PaymentViewModel>> GetAllPaymentsAsync()
        {
            return await _paymentRepository.GetAllPaymentsAsync();
        }
        public async Task<PaymentViewModel> GetPaymentByPaymentIdAsync(int paymentId)
        {
            return await _paymentRepository.GetPaymentByPaymentIdAsync(paymentId);
        }

        public async Task<ResponsePaymentViewModel> GetPaymentResponseByPaymentIdAsync(int paymentId)
        {
            return await _paymentRepository.GetPaymentResponseByPaymentIdAsync(paymentId);
        }
        public async Task<int> UpdatePaymentAsync(int id, PaymentViewModel model)
        {
            return await _paymentRepository.UpdatePaymentAsync(id, model);
        }

        public async Task<PaymentViewModel> Delete(int paymentId)
        {
            var paymentObj = await _paymentRepository.GetById(paymentId);
            if (paymentObj != null)
            {
                await _paymentRepository.Delete(paymentObj);
            }
            return await _paymentRepository.GetPaymentByPaymentIdAsync(paymentId);
        }
        #endregion

    }
}
