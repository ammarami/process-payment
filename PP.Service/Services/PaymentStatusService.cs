﻿using PP.Entity.ViewModels;
using PP.Repository.Interfaces;
using PP.Service.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PP.Service.Services
{
    public class PaymentStatusService : IPaymentStatusService
    {


        #region Initialization
        private IPaymentStatusRepository _PaymentStatusRepository;
        public PaymentStatusService(IPaymentStatusRepository PaymentStatusRepository)
        {
            _PaymentStatusRepository = PaymentStatusRepository;

        }
        #endregion

        #region CRUD
        public async Task<int> AddPaymentStatusAsync(PaymentStatusViewModel model)
        {
            return await _PaymentStatusRepository.AddPaymentStatusAsync(model);
        }
        public async Task<List<PaymentStatusViewModel>> GetAllPaymentStatussAsync()
        {
            return await _PaymentStatusRepository.GetAllPaymentStatussAsync();
        }
        public async Task<PaymentStatusViewModel> GetPaymentStatusByPaymentStatusIdAsync(int PaymentStatusId)
        {
            return await _PaymentStatusRepository.GetPaymentStatusByPaymentStatusIdAsync(PaymentStatusId);
        }
        public async Task<int> UpdatePaymentStatusAsync(int id, PaymentStatusViewModel model)
        {
            return await _PaymentStatusRepository.UpdatePaymentStatusAsync(id, model);
        }
        #endregion

    }
}
