﻿using PP.Entity.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PP.Service.Interfaces
{
    public interface IPaymentStatusService
    {
        #region CRUD
        Task<int> AddPaymentStatusAsync(PaymentStatusViewModel model);
        Task<List<PaymentStatusViewModel>> GetAllPaymentStatussAsync();
        Task<PaymentStatusViewModel> GetPaymentStatusByPaymentStatusIdAsync(int PaymentStatusId);
        Task<int> UpdatePaymentStatusAsync(int Id, PaymentStatusViewModel PaymentStatus);
        #endregion

    }
}
