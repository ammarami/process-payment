﻿using PP.Entity.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PP.Service.Interfaces
{
    public interface IPaymentService
    {
        #region CRUD
        Task<ResponsePaymentViewModel> AddPaymentAsync(RequestPaymentViewModel model);
        Task<List<PaymentViewModel>> GetAllPaymentsAsync();
        Task<PaymentViewModel> GetPaymentByPaymentIdAsync(int paymentId);
        Task<ResponsePaymentViewModel> GetPaymentResponseByPaymentIdAsync(int paymentId);
        Task<int> UpdatePaymentAsync(int Id, PaymentViewModel payment);
        Task<PaymentViewModel> Delete(int paymentId);
        #endregion


    }
}
