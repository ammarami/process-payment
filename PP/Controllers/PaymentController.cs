﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using PP.Entity.ViewModels;
using PP.Service.Interfaces;

namespace SenSights_API.Controllers
{
    /// <summary>
    /// Main web Api controller with "ProcessPayment" Method
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        #region Initialization

        private IPaymentService _paymentService;
        public PaymentController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        #endregion

        #region APIs

        // GET: api/Payment/{Id}
        [HttpGet("{Id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(PaymentViewModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get([FromRoute] int Id)
        {
            try
            {
                var response = await _paymentService.GetPaymentByPaymentIdAsync(Id);
                return Ok(response);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        // GET: api/PaymentsHistory
        [HttpGet("PaymentsHistory")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(PaymentViewModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllPaymentsAsync()
        {
            try
            {
                var response = await _paymentService.GetAllPaymentsAsync();
                return Ok(response);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        // POST: api/ProcessPayment
        [HttpPost("ProcessPayment")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponsePaymentViewModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> ProcessPayment([FromBody] RequestPaymentViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ResponsePaymentViewModel paymentResult = await _paymentService.AddPaymentAsync(model);
                    if (paymentResult != null)
                    {
                        return new OkObjectResult(paymentResult);
                    }
                    else {
                        return BadRequest();
                    }
                }
                return Conflict();
            }
            catch (Exception)
            {

                return BadRequest();

            }
           
        }

        // PUT: api/Payment
        [HttpDelete("{Id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(PaymentViewModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] int Id)
        {
            var type = typeof(BadRequestObjectResult);
            if (ModelState.IsValid)
            {
                return new OkObjectResult(await _paymentService.Delete(Id));
            }
            return Conflict();
        }

        #endregion

    }
}